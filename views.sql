--1
CREATE VIEW q1 AS
    select count(1), receipt_item.brandCode from receipt INNER JOIN receipt_item ON receipt.receipt_item_id = receipt_item.id where receipt_item.brandCode is not null AND receipt_item.brandCode != 'BRAND' AND YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021 AND  MONTH(DATE(FROM_UNIXTIME(CAST(createDate AS INT)/1000))) = 2 GROUP BY receipt_item.brandCode ORDER BY count(1) DESC;
--2
CREATE VIEW q2 AS
    select count(1) As COUNT, receipt_item.brandCode, MONTH(DATE(FROM_UNIXTIME(CAST(createDate AS INT)/1000))) AS MONTH from receipt INNER JOIN receipt_item ON receipt.receipt_item_id = receipt_item.id where receipt_item.brandCode is not null AND receipt_item.brandCode != 'BRAND' AND YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021 GROUP BY brandCode, MONTH(DATE(FROM_UNIXTIME(CAST(createDate AS INT)/1000))) ORDER BY MONTH(DATE(FROM_UNIXTIME(CAST(createDate AS INT)/1000))) DESC, count(1) DESC LIMIT 6;
--3
CREATE VIEW q3 AS
    select AVG(totalSpent) AS AVERAGE_SPEND, rewardsReceiptStatus from receipt where totalSpent is not null AND (rewardsReceiptStatus = 'FINISHED' OR rewardsReceiptStatus = 'REJECTED') GROUP BY rewardsReceiptStatus;
--4
CREATE VIEW q4 AS
    select SUM(purchasedItemCount) AS ITEMS_PURCHASED, rewardsReceiptStatus from receipt where purchasedItemCount is not null AND (rewardsReceiptStatus = 'FINISHED' OR rewardsReceiptStatus = 'REJECTED') GROUP BY rewardsReceiptStatus;
--5
month_year = 2020*12+8
CREATE VIEW q5 AS
    select SUM(receipt.totalSpent) AS SPEND, receipt_item.brandCode from receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id INNER JOIN user ON receipt.userId = user._id where receipt_item.brandCode is not null AND receipt_item.brandCode != 'BRAND' AND (YEAR(DATE(FROM_UNIXTIME(CAST(user.createdDate AS INT)/1000)))*12 + MONTH(DATE(FROM_UNIXTIME(CAST(user.createdDate AS INT)/1000)))) > month_year AND receipt_item.id is not null GROUP BY brandCode ORDER BY SPEND DESC;
--6
CREATE VIEW q6 AS
    select count(1) AS COUNT, receipt_item.brandCode from receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id INNER JOIN user ON receipt.userId = user._id where receipt_item.brandCode is not null AND receipt_item.brandCode != 'BRAND' AND (YEAR(DATE(FROM_UNIXTIME(CAST(user.createdDate AS INT)/1000)))*12 + MONTH(DATE(FROM_UNIXTIME(CAST(user.createdDate AS INT)/1000)))) > month_year AND receipt_item.id is not null GROUP BY brandCode ORDER BY COUNT DESC;

