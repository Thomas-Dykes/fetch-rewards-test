import json

import mysql.connector
import brands
import users
import receipts

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    #   password="yourpassword",
    database="fetch_rewards_request",
)
#loads all the data for all tables
def etl():
    brands.load_brands()
    users.load_users()
    receipts.load_receipts()

    
if __name__ == '__main__':
    etl()