--Data Analysis
CREATE TABLE counts(
    name varchar(30),
    count int(9),
    prop decimal(4,3),
    count_this_year int(9),
    prop_this_year decimal(4,3)
);

select sum(case when barcode is null then 0 else 1 end) count, sum(case when barcode is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when barcode is null then 0 else 1 end) count_this_year, sum(case when barcode is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("barcode", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when brandCode is null then 0 else 1 end) count, sum(case when brandCode is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when brandCode is null then 0 else 1 end) count_this_year, sum(case when brandCode is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("brandCode", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when competitiveProduct is null then 0 else 1 end) count, sum(case when competitiveProduct is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when competitiveProduct is null then 0 else 1 end) count_this_year, sum(case when competitiveProduct is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("competitiveProduct", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when competitorRewardsGroup is null then 0 else 1 end) count, sum(case when competitorRewardsGroup is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when competitorRewardsGroup is null then 0 else 1 end) count_this_year, sum(case when competitorRewardsGroup is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("competitorRewardsGroup", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when deleted is null then 0 else 1 end) count, sum(case when deleted is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when deleted is null then 0 else 1 end) count_this_year, sum(case when deleted is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("deleted", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when description is null then 0 else 1 end) count, sum(case when description is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when description is null then 0 else 1 end) count_this_year, sum(case when description is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("description", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when discountedItemPrice is null then 0 else 1 end) count, sum(case when discountedItemPrice is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when discountedItemPrice is null then 0 else 1 end) count_this_year, sum(case when discountedItemPrice is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("discountedItemPrice", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when finalPrice is null then 0 else 1 end) count, sum(case when finalPrice is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when finalPrice is null then 0 else 1 end) count_this_year, sum(case when finalPrice is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("finalPrice", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when itemNumber is null then 0 else 1 end) count, sum(case when itemNumber is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when itemNumber is null then 0 else 1 end) count_this_year, sum(case when itemNumber is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("itemNumber", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when itemPrice is null then 0 else 1 end) count, sum(case when itemPrice is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when itemPrice is null then 0 else 1 end) count_this_year, sum(case when itemPrice is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("itemPrice", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when metabriteCampaignId is null then 0 else 1 end) count, sum(case when metabriteCampaignId is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when metabriteCampaignId is null then 0 else 1 end) count_this_year, sum(case when metabriteCampaignId is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("metabriteCampaignId", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when needsFetchReview is null then 0 else 1 end) count, sum(case when needsFetchReview is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when needsFetchReview is null then 0 else 1 end) count_this_year, sum(case when needsFetchReview is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("needsFetchReview", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when needsFetchReviewReason is null then 0 else 1 end) count, sum(case when needsFetchReviewReason is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when needsFetchReviewReason is null then 0 else 1 end) count_this_year, sum(case when needsFetchReviewReason is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("needsFetchReviewReason", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when originalFinalPrice is null then 0 else 1 end) count, sum(case when originalFinalPrice is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when originalFinalPrice is null then 0 else 1 end) count_this_year, sum(case when originalFinalPrice is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("originalFinalPrice", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when originalMetaBriteBarcode is null then 0 else 1 end) count, sum(case when originalMetaBriteBarcode is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when originalMetaBriteBarcode is null then 0 else 1 end) count_this_year, sum(case when originalMetaBriteBarcode is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("originalMetaBriteBarcode", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when originalMetaBriteDescription is null then 0 else 1 end) count, sum(case when originalMetaBriteDescription is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when originalMetaBriteDescription is null then 0 else 1 end) count_this_year, sum(case when originalMetaBriteDescription is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("originalMetaBriteDescription", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when originalMetaBriteItemPrice is null then 0 else 1 end) count, sum(case when originalMetaBriteItemPrice is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when originalMetaBriteItemPrice is null then 0 else 1 end) count_this_year, sum(case when originalMetaBriteItemPrice is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("originalMetaBriteItemPrice", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when originalMetaBriteQuantityPurchased is null then 0 else 1 end) count, sum(case when originalMetaBriteQuantityPurchased is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when originalMetaBriteQuantityPurchased is null then 0 else 1 end) count_this_year, sum(case when originalMetaBriteQuantityPurchased is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("originalMetaBriteQuantityPurchased", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when originalReceiptItemText is null then 0 else 1 end) count, sum(case when originalReceiptItemText is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when originalReceiptItemText is null then 0 else 1 end) count_this_year, sum(case when originalReceiptItemText is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("originalReceiptItemText", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when partnerItemId is null then 0 else 1 end) count, sum(case when partnerItemId is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when partnerItemId is null then 0 else 1 end) count_this_year, sum(case when partnerItemId is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("partnerItemId", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when pointsEarned is null then 0 else 1 end) count, sum(case when pointsEarned is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when pointsEarned is null then 0 else 1 end) count_this_year, sum(case when pointsEarned is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("pointsEarned", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when pointsNotAwardedReason is null then 0 else 1 end) count, sum(case when pointsNotAwardedReason is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when pointsNotAwardedReason is null then 0 else 1 end) count_this_year, sum(case when pointsNotAwardedReason is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("pointsNotAwardedReason", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when pointsPayerId is null then 0 else 1 end) count, sum(case when pointsPayerId is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when pointsPayerId is null then 0 else 1 end) count_this_year, sum(case when pointsPayerId is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("pointsPayerId", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when preventTargetGapPoints is null then 0 else 1 end) count, sum(case when preventTargetGapPoints is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when preventTargetGapPoints is null then 0 else 1 end) count_this_year, sum(case when preventTargetGapPoints is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("preventTargetGapPoints", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when priceAfterCoupon is null then 0 else 1 end) count, sum(case when priceAfterCoupon is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when priceAfterCoupon is null then 0 else 1 end) count_this_year, sum(case when priceAfterCoupon is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("priceAfterCoupon", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when quantityPurchased is null then 0 else 1 end) count, sum(case when quantityPurchased is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when quantityPurchased is null then 0 else 1 end) count_this_year, sum(case when quantityPurchased is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("quantityPurchased", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when rewardsGroup is null then 0 else 1 end) count, sum(case when rewardsGroup is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when rewardsGroup is null then 0 else 1 end) count_this_year, sum(case when rewardsGroup is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("rewardsGroup", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when rewardsGroupPartnerId is null then 0 else 1 end) count, sum(case when rewardsGroupPartnerId is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when rewardsGroupPartnerId is null then 0 else 1 end) count_this_year, sum(case when rewardsGroupPartnerId is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("rewardsGroupPartnerId", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when targetPrice is null then 0 else 1 end) count, sum(case when targetPrice is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when targetPrice is null then 0 else 1 end) count_this_year, sum(case when targetPrice is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("targetPrice", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when userFlaggedBarcode is null then 0 else 1 end) count, sum(case when userFlaggedBarcode is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when userFlaggedBarcode is null then 0 else 1 end) count_this_year, sum(case when userFlaggedBarcode is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("userFlaggedBarcode", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when userFlaggedDescription is null then 0 else 1 end) count, sum(case when userFlaggedDescription is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when userFlaggedDescription is null then 0 else 1 end) count_this_year, sum(case when userFlaggedDescription is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("userFlaggedDescription", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when userFlaggedNewItem is null then 0 else 1 end) count, sum(case when userFlaggedNewItem is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when userFlaggedNewItem is null then 0 else 1 end) count_this_year, sum(case when userFlaggedNewItem is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("userFlaggedNewItem", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when userFlaggedPrice is null then 0 else 1 end) count, sum(case when userFlaggedPrice is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when userFlaggedPrice is null then 0 else 1 end) count_this_year, sum(case when userFlaggedPrice is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("userFlaggedPrice", @count, @prop, @count_this_year, @prop_this_year);

select sum(case when userFlaggedQuantity is null then 0 else 1 end) count, sum(case when userFlaggedQuantity is null then 0 else 1 end)/COUNT(1) INTO @count, @prop FROM receipt_item;
select sum(case when userFlaggedQuantity is null then 0 else 1 end) count_this_year, sum(case when userFlaggedQuantity is null then 0 else 1 end)/COUNT(1) prop_this_year INTO @count_this_year, @prop_this_year FROM receipt_item INNER JOIN receipt ON receipt_item.id = receipt.receipt_item_id WHERE YEAR(DATE(FROM_UNIXTIME(CAST(finishedDate AS INT)/1000))) = 2021;
INSERT INTO counts VALUES("userFlaggedQuantity", @count, @prop, @count_this_year, @prop_this_year);

select * from counts ORDER BY prop DESC;
select * from counts ORDER BY prop_this_year DESC;

select MAX(quantityPurchased), MIN(quantityPurchased), AVG(quantityPurchased) from receipt_item;
select MAX(pointsEarned), MIN(pointsEarned), AVG(pointsEarned) from receipt_item;
select MAX(CAST(itemPrice AS DECIMAL(6,2))), MIN(CAST(itemPrice AS DECIMAL(6,2))), AVG(CAST(itemPRice AS DECIMAL(6,2))) FROM receipt_item;
select MAX(CAST(finalPrice AS DECIMAL(6,2))), MIN(CAST(finalPrice AS DECIMAL(6,2))), AVG(CAST(finalPrice AS DECIMAL(6,2))) FROM receipt_item;
select finalPrice, itemPrice from receipt_item;