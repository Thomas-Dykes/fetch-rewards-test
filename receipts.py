import json

import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    #   password="yourpassword",
    database="fetch_rewards_request",
)

mycursor = mydb.cursor()

#loads one row of the receipt table
def add_receipt(
    id,
    bonusPointsEarned,
    bonusPointsEarnedReason,
    createDate,
    dateScanned,
    finishedDate,
    modifyDate,
    pointsAwardedDate,
    pointsEarned,
    purchaseDate,
    purchasedItemCount,
    receipt_item_id,
    rewardsReceiptStatus,
    totalSpent,
    userId
):
    sql = "INSERT INTO receipt(_id, bonusPointsEarned, bonusPointsEarnedReason, createDate, dateScanned, finishedDate, modifyDate, pointsAwardedDate, pointsEarned, purchaseDate, purchasedItemCount, receipt_item_id, rewardsReceiptStatus, totalSpent, userId) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
    params = [
        id,
        bonusPointsEarned,
        bonusPointsEarnedReason,
        createDate,
        dateScanned,
        finishedDate,
        modifyDate,
        pointsAwardedDate,
        pointsEarned,
        purchaseDate,
        purchasedItemCount,
        receipt_item_id,
        rewardsReceiptStatus,
        totalSpent,
        userId
    ]
    mycursor.execute(sql, params)
    mydb.commit()

#loads one row of the receipt_item table
def add_receipt_item(
    barcode,
    brandCode,
    competitiveProduct,
    competitorRewardsGroup,
    deleted,
    description,
    discountedItemPrice,
    finalPrice,
    itemNumber,
    itemPrice,
    metabriteCampaignId,
    needsFetchReview,
    needsFetchReviewReason,
    originalFinalPrice,
    originalMetaBriteBarcode,
    originalMetaBriteDescription,
    originalMetaBriteItemPrice,
    originalMetaBriteQuantityPurchased,
    originalReceiptItemText,
    partnerItemId,
    pointsEarned,
    pointsNotAwardedReason,
    pointsPayerId,
    preventTargetGapPoints,
    priceAfterCoupon,
    quantityPurchased,
    rewardsGroup,
    rewardsGroupPartnerId,
    targetPrice,
    userFlaggedBarcode,
    userFlaggedDescription,
    userFlaggedNewItem,
    userFlaggedPrice,
    userFlaggedQuantity,
):
    sql = "INSERT INTO receipt_item(barcode, brandCode, competitiveProduct, competitorRewardsGroup, deleted, description, discountedItemPrice, finalPrice, itemNumber, itemPrice, metabriteCampaignId, needsFetchReview, needsFetchReviewReason, originalFinalPrice, originalMetaBriteBarcode, originalMetaBriteDescription, originalMetaBriteItemPrice, originalMetaBriteQuantityPurchased, originalReceiptItemText, partnerItemId, pointsEarned, pointsNotAwardedReason, pointsPayerId, preventTargetGapPoints, priceAfterCoupon, quantityPurchased, rewardsGroup, rewardsGroupPartnerId, targetPrice, userFlaggedBarcode, userFlaggedDescription, userFlaggedNewItem, userFlaggedPrice, userFlaggedQuantity) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    params = [
        barcode,
        brandCode,
        competitiveProduct,
        competitorRewardsGroup,
        deleted,
        description,
        discountedItemPrice,
        finalPrice,
        itemNumber,
        itemPrice,
        metabriteCampaignId,
        needsFetchReview,
        needsFetchReviewReason,
        originalFinalPrice,
        originalMetaBriteBarcode,
        originalMetaBriteDescription,
        originalMetaBriteItemPrice,
        originalMetaBriteQuantityPurchased,
        originalReceiptItemText,
        partnerItemId,
        pointsEarned,
        pointsNotAwardedReason,
        pointsPayerId,
        preventTargetGapPoints,
        priceAfterCoupon,
        quantityPurchased,
        rewardsGroup,
        rewardsGroupPartnerId,
        targetPrice,
        userFlaggedBarcode,
        userFlaggedDescription,
        userFlaggedNewItem,
        userFlaggedPrice,
        userFlaggedQuantity,
    ]
    mycursor.execute(sql, params)
    mydb.commit()

#loads the receipt and the receipt_item table.  A row is only added to the receipt_item table if there is a not NULL value in the rewardsReceiptItemList
def load_receipts():
    receipt_list_count = 1
    with open("./receipts.json/receipts.json", "r") as f:
        for line in f.readlines():
            id = json.loads(line)["_id"]["$oid"]
            try:
                bonusPointsEarned = json.loads(line)["bonusPointsEarned"]
            except KeyError:
                bonusPointsEarned = None
            try:
                bonusPointsEarnedReason = json.loads(line)["bonusPointsEarnedReason"]
            except KeyError:
                bonusPointsEarnedReason = None
            createDate = json.loads(line)["createDate"]["$date"]
            dateScanned = json.loads(line)["dateScanned"]["$date"]
            try:
                finishedDate = json.loads(line)["finishedDate"]["$date"]
            except KeyError:
                finishedDate = None
            modifyDate = json.loads(line)["modifyDate"]["$date"]
            try:
                pointsAwardedDate = json.loads(line)["pointsAwardedDate"]["$date"]
            except KeyError:
                pointsAwardedDate = None
            try:
                pointsEarned = json.loads(line)["pointsEarned"]
            except KeyError:
                pointsEarned = None
            try:
                purchaseDate = json.loads(line)["purchaseDate"]["$date"]
            except KeyError:
                purchaseDate = None
            try:
                purchasedItemCount = json.loads(line)["purchasedItemCount"]
            except KeyError:
                purchasedItemCount = None
            rewardsReceiptStatus = json.loads(line)["rewardsReceiptStatus"]
            try:
                totalSpent = json.loads(line)["totalSpent"]
            except KeyError:
                totalSpent = None
            userId = json.loads(line)["userId"]
            # receipt_item starts here
            missing_var_count = 0
            try:
                barcode = json.loads(line)["rewardsReceiptItemList"][0]["barCode"]
            except KeyError:
                barcode = None
                missing_var_count += 1
            try:
                brandCode = json.loads(line)["rewardsReceiptItemList"][0]["brandCode"]
            except KeyError:
                brandCode = None
                missing_var_count += 1
            try:
                competitiveProduct = json.loads(line)["rewardsReceiptItemList"][0]["competitiveProduct"]
            except KeyError:
                competitiveProduct = None
                missing_var_count += 1
            try:
                competitorRewardsGroup = json.loads(line)["rewardsReceiptItemList"][0]["competitorRewardsGroup"]
            except KeyError:
                competitorRewardsGroup = None
                missing_var_count += 1
            try:
                deleted = json.loads(line)["rewardsReceiptItemList"][0]["deleted"]
            except KeyError:
                deleted = None
                missing_var_count += 1
            try:
                description = json.loads(line)["rewardsReceiptItemList"][0]["description"]
            except KeyError:
                description = None
                missing_var_count += 1
            try:
                discountedItemPrice = json.loads(line)["rewardsReceiptItemList"][0]["discountedItemPrice"]
            except KeyError:
                discountedItemPrice = None
                missing_var_count += 1
            try:
                finalPrice = json.loads(line)["rewardsReceiptItemList"][0]["finalPrice"]
            except KeyError:
                finalPrice = None
                missing_var_count += 1
            try:
                itemNumber = json.loads(line)["rewardsReceiptItemList"][0]["itemNumber"]
            except KeyError:
                itemNumber = None
                missing_var_count += 1
            try:
                itemPrice = json.loads(line)["rewardsReceiptItemList"][0]["itemPrice"]
            except KeyError:
                itemPrice = None
                missing_var_count += 1
            try:
                metabriteCampaignId = json.loads(line)["rewardsReceiptItemList"][0]["metabriteCampaignId"]
            except KeyError:
                metabriteCampaignId = None
                missing_var_count += 1
            try:
                needsFetchReview = json.loads(line)["rewardsReceiptItemList"][0]["needsFetchReview"]
            except KeyError:
                needsFetchReview = None
                missing_var_count += 1
            try:
                needsFetchReviewReason = json.loads(line)["rewardsReceiptItemList"][0][
                "needsFetchReviewReason"
                ]
            except KeyError:
                needsFetchReviewReason = None
                missing_var_count += 1
            try:
                originalFinalPrice = json.loads(line)["rewardsReceiptItemList"][0][
                    "originalFinalPrice"
                ]
            except KeyError:
                originalFinalPrice = None
                missing_var_count += 1
            try:
                originalMetaBriteBarcode = json.loads(line)["rewardsReceiptItemList"][0][
                    "originalMetaBriteBarcode"
                ]
            except KeyError:
                originalMetaBriteBarcode = None
                missing_var_count += 1
            try:
                originalMetaBriteDescription = json.loads(line)["rewardsReceiptItemList"][
                    0
                ]["originalMetaBriteDescription"]
            except KeyError:
                originalMetaBriteDescription = None
                missing_var_count += 1
            try:
                originalMetaBriteItemPrice = json.loads(line)["rewardsReceiptItemList"][0][
                    "originalMetaBriteItemPrice"
                ]
            except KeyError:
                originalMetaBriteItemPrice = None
                missing_var_count += 1
            try:
                originalMetaBriteQuantityPurchased = json.loads(line)[
                    "rewardsReceiptItemList"
                ][0]["originalMetaBriteQuantityPurchased"]
            except KeyError:
                originalMetaBriteQuantityPurchased = None
                missing_var_count += 1
            try:
                originalReceiptItemText = json.loads(line)["rewardsReceiptItemList"][0][
                    "originalReceiptItemText"
                ]
            except KeyError:
                originalReceiptItemText = None
                missing_var_count += 1
            try:
                partnerItemId = json.loads(line)["rewardsReceiptItemList"][0][
                    "partnerItemId"
                ]
            except KeyError:
                partnerItemId = None
                missing_var_count += 1
            try:
                pointsEarned = json.loads(line)["rewardsReceiptItemList"][0]["pointsEarned"]
            except KeyError:
                pointsEarned = None
                missing_var_count += 1
            try:
                pointsNotAwardedReason = json.loads(line)["rewardsReceiptItemList"][0][
                    "pointsNotAwardedReason"
                ]
            except KeyError:
                pointsNotAwardedReason = None
                missing_var_count += 1
            try:
                pointsPayerId = json.loads(line)["rewardsReceiptItemList"][0][
                    "pointsPayerId"
                ]
            except KeyError:
                pointsPayerId = None
                missing_var_count += 1
            try:
                preventTargetGapPoints = json.loads(line)["rewardsReceiptItemList"][0][
                    "preventTargetGapPoints"
                ]
            except KeyError:
                preventTargetGapPoints = None
                missing_var_count += 1
            try:
                priceAfterCoupon = json.loads(line)["rewardsReceiptItemList"][0][
                    "priceAfterCoupon"
                ]
            except KeyError:
                priceAfterCoupon = None
                missing_var_count += 1
            try:
                quantityPurchased = json.loads(line)["rewardsReceiptItemList"][0][
                    "quantityPurchased"
                ]
            except KeyError:
                quantityPurchased = None
                missing_var_count += 1
            try:
                rewardsGroup = json.loads(line)["rewardsReceiptItemList"][0]["rewardsGroup"]
            except KeyError:
                rewardsGroup = None
                missing_var_count += 1
            try:
                rewardsGroupPartnerId = json.loads(line)["rewardsReceiptItemList"][0][
                    "rewardsGroupPartnerId"
                ]
            except KeyError:
                rewardsGroupPartnerId = None
                missing_var_count += 1
            try:
                targetPrice = json.loads(line)["rewardsReceiptItemList"][0]["targetPrice"]
            except KeyError:
                targetPrice = None
                missing_var_count += 1
            try:
                userFlaggedBarcode = json.loads(line)["rewardsReceiptItemList"][0][
                    "userFlaggedBarcode"
                ]
            except KeyError:
                userFlaggedBarcode = None
                missing_var_count += 1
            try:
                userFlaggedDescription = json.loads(line)["rewardsReceiptItemList"][0][
                    "userFlaggedDescription"
                ]
            except KeyError:
                userFlaggedDescription = None
                missing_var_count += 1
            try:
                userFlaggedNewItem = json.loads(line)["rewardsReceiptItemList"][0][
                    "userFlaggedNewItem"
                ]
            except KeyError:
                userFlaggedNewItem = None
                missing_var_count += 1
            try:
                userFlaggedPrice = json.loads(line)["rewardsReceiptItemList"][0][
                    "userFlaggedPrice"
                ]
            except KeyError:
                userFlaggedPrice = None
                missing_var_count += 1
            try:
                userFlaggedQuantity = json.loads(line)["rewardsReceiptItemList"][0][
                    "userFlaggedQuantity"
                ]
            except KeyError:
                userFlaggedQuantity = None
                missing_var_count += 1
            if missing_var_count == 34:
                add_receipt(
                id,
                bonusPointsEarned,
                bonusPointsEarnedReason,
                createDate,
                dateScanned,
                finishedDate,
                modifyDate,
                pointsAwardedDate,
                pointsEarned,
                purchaseDate,
                purchasedItemCount,
                None,
                rewardsReceiptStatus,
                totalSpent,
                userId
            )
            else:
                add_receipt_item(
                barcode,
                brandCode,
                competitiveProduct,
                competitorRewardsGroup,
                deleted,
                description,
                discountedItemPrice,
                finalPrice,
                itemNumber,
                itemPrice,
                metabriteCampaignId,
                needsFetchReview,
                needsFetchReviewReason,
                originalFinalPrice,
                originalMetaBriteBarcode,
                originalMetaBriteDescription,
                originalMetaBriteItemPrice,
                originalMetaBriteQuantityPurchased,
                originalReceiptItemText,
                partnerItemId,
                pointsEarned,
                pointsNotAwardedReason,
                pointsPayerId,
                preventTargetGapPoints,
                priceAfterCoupon,
                quantityPurchased,
                rewardsGroup,
                rewardsGroupPartnerId,
                targetPrice,
                userFlaggedBarcode,
                userFlaggedDescription,
                userFlaggedNewItem,
                userFlaggedPrice,
                userFlaggedQuantity,
            )
                add_receipt(
                id,
                bonusPointsEarned,
                bonusPointsEarnedReason,
                createDate,
                dateScanned,
                finishedDate,
                modifyDate,
                pointsAwardedDate,
                pointsEarned,
                purchaseDate,
                purchasedItemCount,
                receipt_list_count,
                rewardsReceiptStatus,
                totalSpent,
                userId
                )
                receipt_list_count += 1

if __name__ == '__main__':
    load_receipts()