import json

import mysql.connector
#starts the connection
mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    #   password="yourpassword",
    database="fetch_rewards_request",
)

mycursor = mydb.cursor()

#loads one row into the brand table
def add_brand(id, name, barcode, brandCode, category, categoryCode, cpg, topBrand):
    sql = "INSERT INTO brand(_id, name, barcode, brandCode, category, categoryCode, cpg, topbrand) VALUES(%s, %s, %s, %s, %s, %s, %s, %s)"
    params = (id, name, barcode, brandCode, category, categoryCode, str(cpg), topBrand)
    mycursor.execute(sql, params)
    mydb.commit()
#loads the brand table
def load_brands():
    with open("./brands.json/brands.json", "r") as f:
        for line in f.readlines():
            id = json.loads(line)["_id"]["$oid"]
            name = json.loads(line)["name"]
            barcode = json.loads(line)["barcode"]
            try:
                brandCode = json.loads(line)["brandCode"]
            except KeyError:
                brandCode = None
            try:
                category = json.loads(line)["category"]
            except KeyError:
                category = None
            try:
                categoryCode = json.loads(line)["categoryCode"]
            except KeyError:
                categoryCode = None
            cpg = json.loads(line)["cpg"]
            try:
                topBrand = json.loads(line)["topBrand"]
            except KeyError:
                topBrand = None
            add_brand(id, name, barcode, brandCode, category, categoryCode, cpg, topBrand)

if __name__ == '__main__':
    load_brands()
