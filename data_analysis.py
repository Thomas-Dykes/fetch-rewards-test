import pandas as pd
import matplotlib.pyplot as plt
import json

import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    #   password="yourpassword",
    database="fetch_rewards_request",
)

mycursor = mydb.cursor()
#Generates two plots, one for proportion of items in a list overall, and one for proportion of items in a list this year.
def generate_plots():
    string = "SELECT name, prop FROM counts ORDER BY prop DESC LIMIT 10;"
    query = pd.read_sql_query(string, mydb)
    data = pd.DataFrame(query)
    data["prop"] = data["prop"] * 100
    plt.figure(figsize=(10,8))
    plt.bar(data["name"], data["prop"])
    plt.title("Percentage of Times Found in Receipt List")
    plt.xlabel("Receipt Item")
    plt.xticks(rotation=20)
    plt.savefig("prop.png")
    plt.show()

    string = "SELECT name, prop_this_year FROM counts ORDER BY prop_this_year DESC LIMIT 10;"
    query = pd.read_sql_query(string, mydb)
    data = pd.DataFrame(query)
    data["prop_this_year"] = data["prop_this_year"] * 100
    plt.figure(figsize=(10,8))
    plt.title("Percentage of Times Found in Receipt List This Year")
    plt.xlabel("Receipt Item")
    plt.bar(data["name"], data["prop_this_year"])
    plt.xticks(rotation=20)
    plt.savefig("prop_this_year.png")
    plt.show()

if __name__ == '__main__':
    generate_plots()
