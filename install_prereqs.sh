
set -exou pipefail

echo "set variables"
DB_USER="root"
HOST="localhost"
DB="fetch_rewards_request"
echo "variables: DB_USER: $DB_USER; HOST: $HOST; DB: $DB" 

#Installs python, mysql, and other technologies needed for analysis.
sudo yum install python3.x86_64 -y || echo "python3.x86_64 failed to install"
sudo yum install python3-devel.x86_64 -y || echo "python3 dev tools failed to install"
pip3 install pymysql || echo "pymysql failed to install"
pip3 install mysql-connector==2.1.7 || echo "mysql-connector failed to install"
pip3 install numpy || echo "numpy failed"
pip3 install matplotlib || echo "matplotlib failed"

sudo systemctl start mariadb || echo "mariadb failed to start"
sudo systemctl enable mariadb || echo "mariadb could not be registeristed to start on start up"

mysql -h $HOST -u $DB_USER -e "CREATE DATABASE IF NOT EXISTS "$DB";"
