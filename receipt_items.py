import json

import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    #   password="yourpassword",
    database="fetch_rewards_request",
)

#prints a list of all columsn in rewardsReceiptItemList
mycursor = mydb.cursor()
receipt_items = set()
with open("./receipts.json/receipts.json", "r") as f:
    for line in f.readlines():
        try:
            receiptlist = json.loads(line)["rewardsReceiptItemList"]
            for item in receiptlist:
                for k in item.keys():
                    receipt_items.add(k)
        except KeyError:
            receiptlist = {}

print(f"receipt_items: {receipt_items}")
