import json

import mysql.connector

mydb = mysql.connector.connect(
    host="localhost", user="root", database="fetch_rewards_request"
)

mycursor = mydb.cursor()

#Adds one row to the user table
def add_user(id, state, createdDate, lastLogin, role, signUpSource, active):
    # print(f"createdDate: {createdDate}")
    sql = "INSERT INTO user(_id, state, createdDate, lastLogin, role, signUpSource, active) VALUES(%s, %s, %s, %s, %s, %s, %s)"
    params = [id, state, createdDate, lastLogin, role, signUpSource, active]
    mycursor.execute(sql, params)
    mydb.commit()

ids = []
#loads the user table
def load_users():
    with open("./users.json/users.json", "r") as f:
        for line in f.readlines():
            # print(json.loads(line))
            id = json.loads(line)["_id"]["$oid"]
            try:
                state = json.loads(line)["state"]
            except KeyError:
                state = None
            try:
                createdDate = json.loads(line)["createdDate"]["$date"]
            except KeyError:
                createdDate = None
            try:
                lastLogin = json.loads(line)["lastLogin"]["$date"]
            except KeyError:
                lastLogin = None
            role = json.loads(line)["role"]
            try:
                signUpSource = json.loads(line)["signUpSource"]
            except KeyError:
                signUpSource = None
            active = json.loads(line)["active"]
        # print(f"ids: {ids}")
            if (id not in ids):
                add_user(id, state, createdDate, lastLogin, role, signUpSource, active)
                ids.append(id)

if __name__ == '__main__':
    load_users()
